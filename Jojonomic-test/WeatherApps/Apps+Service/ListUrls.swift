//
//  ListUrls.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

let appId = "a402ddcf7609d98f4dfe05ed49f49885"

public struct ListUrls {
    static let GET_WEATHER_DATA = "data/2.5/onecall?lat={lat}&lon={lon}&appid=\(appId)"
}

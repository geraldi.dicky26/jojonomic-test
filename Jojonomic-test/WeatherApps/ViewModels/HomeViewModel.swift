//
//  HomeViewModel.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import RxSwift
import RxCocoa
import SwiftyJSON

class HomeViewModel: NetworkViewModel {
    let disposeBag = DisposeBag()
    
    var error = BehaviorRelay<String?>(value: nil)
    var weatherData = BehaviorRelay<NetworkResponse?>(value: nil)

    var weatherDataResponse = BehaviorRelay<WeatherDataModel?>(value: nil)
    
    required init() {
        super.init()
        registerObserver()
    }
    
    fileprivate func registerObserver() {
        weatherData.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleWeatherResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
    }

    fileprivate func handleWeatherResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        print("ERRORS: \(json)")
        if json["errors"].exists() {
            error.accept(json["errors"][0].stringValue)
        } else {
            weatherDataResponse.accept(WeatherDataModel(json))
        }
    }
    
    func getWeatherByLocation(lat: Double, lon: Double) {
        self.isLoading.accept(true)
        HomeViewDataSource.getUserProfile(long: "\(lat)", lat: "\(lon)", networkResponse: weatherData)
    }
}

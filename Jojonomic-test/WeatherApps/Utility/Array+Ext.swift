//
//  Array+Ext.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import Foundation

public extension Array {
    subscript(safe index: Int) -> Element? {
        guard index >= 0, index < endIndex else {
            return nil
        }
        
        return self[index]
    }
}


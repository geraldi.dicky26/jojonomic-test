//
//  Optional+Ext.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import Foundation

public extension Optional {
    func ifNil(_ then: Wrapped) -> Wrapped {
        switch self {
        case .none: return then
        case let .some(value): return value
        }
    }
}

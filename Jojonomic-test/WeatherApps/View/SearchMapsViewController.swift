//
//  SearchMapsViewController.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 16/06/21.
//

import UIKit

class SearchMapsViewController: BaseNetworkViewController<HomeViewModel> {
    @IBOutlet weak var searchBarField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var outsideButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
    }
}

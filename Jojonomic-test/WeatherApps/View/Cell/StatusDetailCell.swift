//
//  StatusDetailCell.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import UIKit

class StatusDetailCell: UITableViewCell {
    @IBOutlet weak var windStatusLabel: UILabel!
    @IBOutlet weak var humidityLevelLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    
    func bindView(data: CurrentWeatherModel?) {
        guard let data = data else { return }
        
        windStatusLabel.text = "\(data.wingSpeed.ifNil(0))"
        humidityLevelLabel.text = "\(data.humidity.ifNil(0))"
        visibilityLabel.text = "\(data.visibility.ifNil(0))"
    }
}

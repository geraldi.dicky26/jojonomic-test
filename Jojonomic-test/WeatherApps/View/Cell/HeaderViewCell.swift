//
//  HeaderViewCell.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import UIKit

class HeaderViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var temperatureDegreeLabel: UILabel!
    @IBOutlet weak var temperatureDegreeFeelsLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var weatherSubtitleLabel: UILabel!

    func bindView(data: WeatherDetailModel?, current: CurrentWeatherModel?) {
        guard let data = data, let current = current else {
            return
        }
        if let url = URL(string: "http://openweathermap.org/img/wn/\(data.icon.ifNil(""))@2x.png") {
            iconImageView.download(from: url)
        }

        temperatureDegreeLabel.text = "\(String(format: "%.0f", current.temp.ifNil(0) - 273))"
        temperatureDegreeFeelsLabel.text = "Feels like \(String(format: "%.0f",current.feelsLike.ifNil(0) - 273))"
        weatherLabel.text = "\(data.main.ifNil(""))"
        weatherSubtitleLabel.text = "\(data.description.ifNil(""))"
    }
}

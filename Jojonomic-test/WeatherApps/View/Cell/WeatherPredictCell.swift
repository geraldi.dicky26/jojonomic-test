//
//  WeatherPredictCell.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import UIKit

class WeatherPredictCell: UITableViewCell {
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    func bindView(data: WeatherDetailModel?, date: String) {
        dateTime.text = date
        statusLabel.text = data?.main
        if let url = URL(string: "http://openweathermap.org/img/wn/\((data?.icon).ifNil(""))@2x.png") {
            iconImageView.download(from: url)
        }
    }
}

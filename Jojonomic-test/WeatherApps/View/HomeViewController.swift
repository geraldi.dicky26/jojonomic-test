//
//  HomeViewController.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import UIKit
import SwifterSwift
import MapKit
import CoreLocation

class HomeViewController: BaseNetworkViewController<HomeViewModel> {
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!

    private var locationManager:CLLocationManager!
    private var userLong = 0.0
    private var userLat = 0.0

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        loadingView.startAnimating()
        locationManager = CLLocationManager()
        locationManager.delegate = self

        setupTableView()
    }
    
    override func registerObserver() {
        viewModel?.weatherDataResponse.subscribe(onNext: { [weak self] _ in
            guard let vc = self else {
                return
            }
            
            vc.loadingView.stopAnimating()
            vc.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        viewModel?.getWeatherByLocation(lat: userLat, lon: userLong)
        determineMyCurrentLocation()
    }
}

// MARK: - Get Location
extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        userLong = userLocation.coordinate.latitude
        userLat = userLocation.coordinate.longitude
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            guard let placemark = placemarks else {
                return
            }
            
            if placemark.count > 0 {
                guard let placemark = placemarks?[safe: 0] else { return }
                self.title = "\(placemark.administrativeArea!), \(placemark.locality!)"
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}

// MARK: - Function
extension HomeViewController {
    private func setupTableView() {
        tableView.estimatedRowHeight = 100
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib(nibName: "HeaderViewCell", bundle: nil), forCellReuseIdentifier: "HeaderViewCell")
        tableView.register(UINib(nibName: "StatusDetailCell", bundle: nil), forCellReuseIdentifier: "StatusDetailCell")
        tableView.register(UINib(nibName: "WeatherPredictCell", bundle: nil), forCellReuseIdentifier: "WeatherPredictCell")
        
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
}

// MARK: - TableView Delegate
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = viewModel?.weatherDataResponse.value
        
        if indexPath.row == 0 {
            let headerViewCell = tableView.dequeueReusableCell(withClass: HeaderViewCell.self, for: indexPath)
            headerViewCell.bindView(data: data?.current?.weatherDetail[safe: 0], current: data?.current)
            headerViewCell.contentView.clipsToBounds = true
            headerViewCell.contentView.layer.cornerRadius = 16
            headerViewCell.selectionStyle = .none
            
            return headerViewCell
        } else if indexPath.row == 1 {
            let statusDetailCell = tableView.dequeueReusableCell(withClass: StatusDetailCell.self, for: indexPath)
            statusDetailCell.bindView(data: data?.current)
            statusDetailCell.selectionStyle = .none
            
            return statusDetailCell
        } else {
            let weatherData = data?.daily[safe: indexPath.row - 2]?.weather[safe: 0]
            let weatherPredictCell = tableView.dequeueReusableCell(withClass: WeatherPredictCell.self, for: indexPath)
            weatherPredictCell.bindView(data: weatherData, date: (data?.daily[safe: indexPath.row - 2]?.date).ifNil(""))
            weatherPredictCell.selectionStyle = .none
            
            return weatherPredictCell
        }
    }
}

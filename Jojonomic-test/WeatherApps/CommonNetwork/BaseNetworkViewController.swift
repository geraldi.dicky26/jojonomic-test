//
//  BaseNetworkViewController.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import UIKit

class BaseNetworkViewController<D: BaseViewModel>: BaseViewController<D> {
    public func hideKeyboard() {
        self.view.endEditing(true)
    }
}


//
//  BaseViewModel.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import RxCocoa

class BaseViewModel: NSObject {
    public var isLoading = BehaviorRelay<Bool>(value: false)
    public var message = BehaviorRelay<String?>(value: nil)
    
    public required override init() {
        super.init()
    }
}


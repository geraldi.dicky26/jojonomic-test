//
//  ViewModelBased.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import UIKit

public protocol ViewModelBased: class {
    associatedtype ViewModel
    var viewModel: ViewModel? { get set }
}


//
//  HomeViewDataSource.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import RxCocoa

class HomeViewDataSource: RemoteDataResource {
    static func getUserProfile(long: String, lat: String, networkResponse: BehaviorRelay<NetworkResponse?>) {
        let url = ListUrls.GET_WEATHER_DATA.replacingOccurrences(of: "{lon}", with: long)

        BaseApi.get(with: url.replacingOccurrences(of: "{lat}", with: lat)) { (response) in
            networkResponse.accept(response)
        }
    }
}

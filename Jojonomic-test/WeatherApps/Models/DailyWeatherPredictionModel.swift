//
//  DailyWeatherPredictionModel.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import SwiftyJSON

class DailyWeatherPredictionModel {
    var date: String?
    var weather: [WeatherDetailModel] = []
    
    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }
        
        date = json["dt"].doubleValue.getDateStringFromUTC()
        for object in json["weather"].arrayValue {
            weather.append(WeatherDetailModel(object))
        }
    }
}

//
//  CurrentWeatherModel.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import SwiftyJSON

class CurrentWeatherModel {
    var dt: String?
    var sunrise: String?
    var sunset: String?
    var temp: Double?
    var feelsLike: Double?
    var pressure: Double?
    var humidity: Int?
    var visibility: Double?
    var wingSpeed: Double?
    var weatherDetail: [WeatherDetailModel] = []
    
    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }
        
        dt = json["dt"].doubleValue.getDateStringFromUTC()
        sunrise = json["sunrice"].doubleValue.getDateStringFromUTC()
        sunset = json["sunset"].doubleValue.getDateStringFromUTC()
        temp = json["temp"].doubleValue
        feelsLike = json["feels_like"].doubleValue
        pressure = json["pressure"].doubleValue
        humidity = json["humidity"].intValue
        visibility = json["visibility"].doubleValue
        wingSpeed = json["wind_speed"].doubleValue
        
        for object in json["weather"].arrayValue {
            weatherDetail.append(WeatherDetailModel(object))
        }
    }
}

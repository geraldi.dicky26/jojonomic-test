//
//  WeatherDetailModel.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import SwiftyJSON

class WeatherDetailModel {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
    
    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }
        
        id = json["id"].intValue
        main = json["main"].stringValue
        description = json["description"].stringValue
        icon = json["icon"].stringValue
    }
}

//
//  WeatherDataModel.swift
//  Jojonomic-test
//
//  Created by Dicky Geraldi on 13/06/21.
//

import SwiftyJSON

class WeatherDataModel {
    var lat: Double?
    var lon: Double?
    var current: CurrentWeatherModel?
    var daily: [DailyWeatherPredictionModel] = []
    
    init(_ json: JSON? = "") {
        guard let json = json else {
            return
        }
        
        lat = json["lat"].doubleValue
        lon = json["lon"].doubleValue
        current = CurrentWeatherModel.init(json["current"])
        
        for object in json["daily"].arrayValue {
            daily.append(DailyWeatherPredictionModel(object))
        }
    }
}
